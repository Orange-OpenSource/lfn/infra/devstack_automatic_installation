---
stages:
  - lint
  - install
  - postconfigure

##
# Variables
##

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  ANSIBLE_DOCKER_IMAGE:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible
  ANSIBLE_DOCKER_TAG: "2.8"
  CHAINED_CI_INIT: scripts/chained-ci-init.sh

.syntax_checking: &syntax_checking
  stage: lint
  except:
    - schedules
    - triggers
    - web
  tags:
    - gitlab-org

yaml_checking:
  image: sdesbure/yamllint:latest
  script:
    - yamllint *.yml
    - yamllint inventory/group_vars/*.yml
  <<: *syntax_checking

ansible_linting:
  image: sdesbure/ansible-lint:latest
  script:
    - ansible-lint -x ANSIBLE0010,ANSIBLE0013 os_*.yml
  <<: *syntax_checking

##
# Generic
##

.ansible_run: &ansible_run
  tags:
    - gitlab-org
  image: ${ANSIBLE_DOCKER_IMAGE}:${ANSIBLE_DOCKER_TAG}
  only:
    - schedules
    - triggers
    - web

.chained_ci_tools_after: &chained_ci_tools_after
  after_script:
    - ./scripts/clean.sh

.chained_ci_tools_before: &chained_ci_tools_before
  <<: *chained_ci_tools_after
  before_script:
    - chmod 600 .
    - . ./${CHAINED_CI_INIT} -a -i inventory/infra
    - ansible-galaxy install -r requirements.yml

.artifacts: &artifacts
  artifacts:
    paths:
      - inventory/infra
      - vars/idf.yml
      - vars/vaulted_ssh_credentials.yml
      - vars/ssh_gateways.yml
      - vars/clouds.yaml
      - vars/openrc

create_devstack:
  stage: install
  <<: *ansible_run
  <<: *chained_ci_tools_before
  <<: *artifacts
  script:
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/infra \
        devstack.yml

postconfigure:
  stage: postconfigure
  <<: *ansible_run
  <<: *chained_ci_tools_before
  <<: *artifacts
  script:
    - >
      ansible-vault decrypt --vault-password-file=.vault \
            vars/openrc || true
    - source vars/openrc
    - >
      ansible-playbook ${ansible_verbose} ${VAULT_OPT} \
        -i inventory/infra \
        postconfigure.yml
