---
- hosts: devstack
  vars_files:
    - "vars/idf.yml"
  tasks:
    - name: "[Debian] Install list of needed packages"
      include_role:
        name: apt_install
      vars:
        packages:
          - git

    - name: allow oom_path parent directory to be usable by user
      become: true
      file:
        path: "{{ devstack_path.split('/')[0:-1] | join('/') }}"
        state: directory
        mode: 0777

    - name: "clone devstack release {{ openstack_release }}"
      git:
          dest: "{{ devstack_path }}"
          repo: https://github.com/openstack/devstack.git
          depth: 1
          version: "{{ openstack_release }}"

    - name: check if devstack configuration file exists
      stat:
          path: "{{ devstack_path }}/local.conf"
      register: stat

    - name: get password from devstack configuration file
      when: stat.stat.exists
      block:
        - name: create temporary local file for local.conf
          tempfile:
            state: file
            suffix: temp
          register: tmp_local
          delegate_to: '127.0.0.1'

        - name: fetch cloud config
          fetch:
            dest: "{{ tmp_local.path }}"
            src: "{{ devstack_path }}/local.conf"
            flat: "yes"

        - name: remove enable_service lines
          lineinfile:
            path: "{{ tmp_local.path }}"
            regexp: '^enable_service'
            state: absent
          delegate_to: '127.0.0.1'

        - name: remove enable_service lines
          lineinfile:
            path: "{{ tmp_local.path }}"
            regexp: '^enable_plugin'
            state: absent
          delegate_to: '127.0.0.1'

        - name: change section name
          lineinfile:
            path: "{{ tmp_local.path }}"
            regexp: '^\[\[local'
            line: '[local]'
          delegate_to: '127.0.0.1'

        - name: retrieve password
          set_fact:
            passwd:
              "{{ lookup(
                    'ini',
                    'ADMIN_PASSWORD section=local file=' + tmp_local.path) }}"

      always:
        - name: destroy the local tmp_local
          file:
            path: "{{ tmp_local.path }}"
            state: absent
          delegate_to: '127.0.0.1'

    - name: "Generate password for OpenStack"
      set_fact:
        passwd:
          "{{ lookup('password', \
          '/dev/null chars=ascii_letters,digits,hexdigits length=22') }}"
      when: not stat.stat.exists

    - name: generate devstack configuration
      template:
        src: templates/local.conf.j2
        dest: "{{ devstack_path }}/local.conf"
      when: not stat.stat.exists

    - name: stack it
      raw: "cd {{ devstack_path }} && ./stack.sh"

- hosts: kube-master
  tasks:
    - name: "[Debian] Install list of needed packages"
      include_role:
        name: apt_install
      vars:
        packages:
          - jq

    - name: generate openrc file
      template:
        src: templates/openstack_rc.j2
        dest: "{{ ansible_user_dir }}/openrc"

    - name: ensure openstack config folder exists
      file:
        path: "{{ ansible_user_dir }}/.config/openstack"
        recurse: "yes"
        state: directory

    - name: generate clouds.yaml file
      template:
        src: templates/clouds.yaml.j2
        dest: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml"

    - name: ensure relevant pip packages are installed
      pip:
        name:
          - openstacksdk==0.43.0
          - os-client-config==2.0.0
          - python-openstackclient==5.1.0
        state: present
      become: true

    - name: get project infos
      os_project_facts:
        cloud: admin
        name: alt_demo

    - name: set project_id
      set_fact:
        project_id: "{{ openstack_projects[0].id }}"

    - name: add project id to openstack_rc
      lineinfile:
          path: "{{ ansible_user_dir }}/openrc"
          regexp: '^export OS_PROJECT_ID='
          line: "export OS_PROJECT_ID={{ project_id }}"

    - name: add project id to clouds.yaml
      lineinfile:
        path: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml"
        regexp: '^[ ]+project_id:'
        insertafter: '^[ ]+project_name:'
        line: "            project_id: {{ project_id }}"

    - name: fetch clouds.yaml
      fetch:
        dest: "{{ playbook_dir }}/vars/clouds.yaml"
        src: "{{ ansible_user_dir }}/.config/openstack/clouds.yaml"
        flat: "yes"

    - name: fetch openrc
      fetch:
        dest: "{{ playbook_dir }}/vars/openrc"
        src: "{{ ansible_user_dir }}/openrc"
        flat: "yes"
